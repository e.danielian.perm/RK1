#include <iostream>
#include <string>
using namespace std;
int main()
{
    string sn;
    getline(cin,sn);
    int n = 0;
    for(int i=0; i<sn.length(); i++)
        n = n*10 + sn[i] - '0';
    string s;
    cin.clear();
    cin.ignore(cin.rdbuf()->in_avail());
    getline(cin,s);
    if(n<=0)
    {
        cout<<"Error";
        return 0;
    }   
    int*a;
    a=new int[n];
    for(int i=0; i<n; i++)
        a[i]=0;
    int i,j=0;
    for(i=0; i<s.length() && j<n; i++)
    {
        if(s[i]>='0' && s[i]<='9')
            a[j]=a[j]*10+s[i]-'0';
        if(s[i]==' ')
        j++;
    }
    j++;
    if(j!=n || i!=s.length())
    {
        cout<<"Error";
        return 0;
    }
    for(i=0; i<n/2; i++)
        swap(a[i],a[n-i-1]);
    for(i = 0; i<n; i++)
        cout<<a[i]<<' ';
    
    return 0;
}